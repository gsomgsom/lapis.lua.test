local md5           = require "md5"
local assert_error  = require("lapis.application").assert_error
local Users         = require "models.users"

return {
	GET = function(self)
		return {
			json = {
				success = false,
				error = true,
				message = "Wrong call!"
			}
		}
	end,
	POST = function(self)
		-- Проверяем данные пользователя
		if self.params.email then
			local user = assert_error(Users:verify_user(self.params))
			if user then
				return {
					json = {
						success = true,
						error = false,
						token = md5.sumhexa(user.password),
					}
				}
			else
				return {
					json = {
						success = false,
						error = true,
						message = "E-mail или пароль указаны неверно."
					}
				}
			end
		end
	end
}
