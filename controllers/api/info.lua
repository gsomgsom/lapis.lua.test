local assert_error  = require("lapis.application").assert_error
local Users         = require "models.users"

return {
	GET = function(self)
		return {
			json = {
				success = false,
				error = true,
				message = "Wrong call!"
			}
		}
	end,
	POST = function(self)
		-- Проверяем данные пользователя
		if self.params.token then
			local user = assert_error(Users:verify_token(self.params.token))
			if user then
				return {
					json = {
						success = true,
						error = false,
						name = user.name,
						email = user.email,
						balance = user.balance,
					}
				}
			else
				return {
					json = {
						success = false,
						error = true,
						message = "Токен не найден!"
					}
				}
			end
		end
		return {
			json = {
				success = false,
				error = true,
				message = "Wrong call!"
			}
		}
	end
}
