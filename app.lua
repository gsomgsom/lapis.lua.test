local lapis       = require "lapis"
local respond_to  = require ("lapis.application").respond_to
local app         = lapis.Application()
app:enable("etlua")
app.layout = require "views.layout"

-- API
local api_login   = require "controllers.api.login"
local api_info    = require "controllers.api.info"
local api_update_info = require "controllers.api.update_info"
local api_operations = require "controllers.api.operations"

-- Public
local code_404    = require "controllers.code_404"
local index       = require "controllers.index"

-- Handle
app.handle_404 = code_404

-- API
app:match("/api/login",                   respond_to(api_login))
app:match("/api/info",                    respond_to(api_info))
app:match("/api/update_info",             respond_to(api_update_info))
app:match("/api/operations",              respond_to(api_operations))
app:match("/404",                         code_404)

-- Public
app:match("/",                            index)
app:match("/404",                         code_404)

return app
