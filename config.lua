local config = require "lapis.config"
local token  = require "secrets.token"

config("development", {
	site_name  = "Lapis LUA test",
	port       = 8080,
	secret     = token,
	mysql      = {
		host     = "127.0.0.1",
		user     = "root",
		password = "123",
		database = "lapis_lua_test"
	},
})

config("production", {
	code_cache = "on",
	site_name  = "Lapis LUA test",
	port       = 80,
	secret     = token,
	mysql      = {
		host     = "127.0.0.1",
		user     = "lapis_lua_test",
		password = "SilverFunnyJapan69",
		database = "lapis_lua_test"
	},
})
