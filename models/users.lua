local md5      = require "md5"
local token    = require "secrets.token"
local trim     = require("lapis.util").trim_filter
local Model    = require("lapis.db.model").Model
local Users    = Model:extend("users")

--- Создадим нового пользователя
-- @tparam table user User data
-- @treturn boolean success
-- @treturn string error
function Users:create_user(user)
	-- Уберём лишние пробелы
	trim(user, {
		"email", "name", "password"
	}, nil)

	-- Сгенерируем хэш
	local hash = md5.sumhexa(user.email .. user.password .. token)

	local u = self:create {
		email    = user.email,
		password = hash,
		name     = user.name,
		balance  = user.balance
	}

	if u then
		return u
	end

	return false, { "err_create_user", { user.email } }
end

--- Обновление данных пользователя
-- @tparam table user User data
-- @treturn boolean success
-- @treturn string error
function Users:modify_user(user)
	local columns = {}
	for col in pairs(user) do
		table.insert(columns, col)
	end

	-- Сгенерируем хэш
	local hash = md5.sumhexa(user.email .. user.password .. token)

	return user:update(unpack(columns))
end

--- Удалим пользователя
-- @tparam table user User data
-- @treturn boolean success
-- @treturn string error
function Users:delete_user(user)
	return user:delete()
end

--- Верификация пользователя
-- @tparam table params User data
-- @treturn boolean success
-- @treturn string error
function Users:verify_user(params)
	local user = self:get_user(params.email)

	-- Пользователей с таким email не найдено
	if not user then
		return false, { "err_invalid_user" }
	end

	-- Сгенерируем хэш для сравнения
	local password = md5.sumhexa(user.email .. params.password .. token)

	-- Сравним пароль
	local verified = (password == user.password)

	if verified then
		return user
	else
		return false, { "err_invalid_user" }
	end
end

--- Верификация пользователя по токену
-- @tparam string username Username
-- @treturn boolean success
-- @treturn string error
function Users:verify_token(token)
	local user = self:get_user_by_token(token)

	-- Пользователей с таким email не найдено
	if not user then
		return false, { "err_invalid_user" }
	end

	-- Сравним пароль
	local verified = (token == md5.sumhexa(user.password))

	if verified then
		return user
	else
		return false, { "err_invalid_user" }
	end
end

--- Получаем всех пользователей
-- @treturn table users List of users
function Users:get_users()
	return self:select("order by email asc")
end

--- Получаем пользователя по email
-- @tparam string username Username
-- @treturn table user
function Users:get_user(email)
	email = string.lower(email)
	return unpack(self:select("where lower(email)=? limit 1", email))
end

--- Получаем пользователя по ID
-- @tparam number id User ID
-- @treturn table user
function Users:get_user_by_id(id)
	return unpack(self:select("where id=? limit 1", id))
end

--- Получаем пользователя по token
-- @tparam string token User token
-- @treturn table user
function Users:get_user_by_token(token)
	return unpack(self:select("where md5(password)=? limit 1", token))
end

return Users
