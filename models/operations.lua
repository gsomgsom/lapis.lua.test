local Model      = require("lapis.db.model").Model
local Operations = Model:extend("operations")

--- Получаем историю операций юзера
-- @treturn table operations List of operations
function Operations:get_operations(user_id)
	return self:select(self:select("where lower(user_id)=? order by timestamp desc", user_id))
end

return Operations
