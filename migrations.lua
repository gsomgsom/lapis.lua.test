local db     = require "lapis.db"
local schema = require "lapis.db.schema"
local types  = schema.types
local Users  = require "models.users"

return {
	-- Наша первая миграция. Опишем структуру и заполним базу данными
	[1510866587] = function()

		-- Таблица "users"
		schema.create_table("users", {
			{ "id",       types.integer { unique=true, primary_key=true, auto_increment=true }},
			{ "email",    types.varchar { unique=true }},
			{ "name",     types.varchar },
			{ "password", types.varchar },
			{ "balance",  types.double { default=0 }}
		})


		-- Таблица "operations"
		schema.create_table("operations", {
			{ "id",            types.integer { unique=true, primary_key=true, auto_increment=true }},
			{ "user_id",       types.integer { null=true, default=false }},
			{ "timestamp",     types.timestamp },
			{ "amount",        types.double { default=0 }}
		})

		-- Создадим пользователя
		local user = Users:create_user({
			email = 'test@test.ru',
			name = 'Jovanny Krot',
			password = 'qwerty',
		})

		-- Данные "operations"
		--@TODO сделать генерацию кучи случайных данных для постраничного вывода
		db.insert("operations", {
			user_id   = user.id,
			timestamp = "2017-11-19 12:23:34",
			amount    = 0.67
		})
		db.insert("operations", {
			user_id   = user.id,
			timestamp ="2017-11-19 15:32:44",
			amount    = 2.11
		})
		db.insert("operations", {
			user_id   = user.id,
			timestamp ="2017-11-19 17:12:21",
			amount    = -1.12
		})

	end
}
