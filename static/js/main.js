var token = '';

(function($) {
	$(document).ready(function() {
		$('.content').load('/static/login.html', function() {
			$('.form-signin').on('submit', function(){
				$.ajax({
					url: '/api/login',
					global: false,
					type: 'POST',
					data: {
						email: $('#inputEmail').val(),
						password: $('#inputPassword').val()
					},
					dataType: 'json',
					success: function(result) {
						if (result.success) {
							token = result.token;
							$('body').load('/static/logged.html', function() {
								// Кабинет
								$('#cabinet').unbind('click').click(function(){
									$('.logged-content').load('/static/cabinet.html', function() {
										$.ajax({
											url: '/api/info',
											global: false,
											type: 'POST',
											data: {
												token: token
											},
											dataType: 'json',
											success: function(result) {
												if (result.success) {
													$('#cabinet_name').text(result.name);
													$('#cabinet_email').text(result.email);
													$('#cabinet_balance').text(result.balance);
													$('#info_edit').unbind('click').click(function(){
															var n = prompt('Введите новое имя');
															if (n) {
																$.ajax({
																	url: '/api/update_info',
																	global: false,
																	type: 'POST',
																	data: {
																		token: token,
																		name: n
																	},
																	dataType: 'json',
																	success: function(result) {
																		if (result.success) {
																			$('#cabinet').click();
																		}
																		else {
																			console.log(result);
																		}
																	},
																	error: function(request, status, error) {
																		console.log('Ошибка: ' + request.responseText);
																	}
																});
															}
													});
												}
												else {
													console.log(result);
												}
											},
											error: function(request, status, error) {
												console.log('Ошибка: ' + request.responseText);
											}
										});
									});
								});

								// История платежей
								$('#history').unbind('click').click(function(){
									$('.logged-content').load('/static/history.html', function() {
										$.ajax({
											url: '/api/operations',
											global: false,
											type: 'POST',
											data: {
												token: token
											},
											dataType: 'json',
											success: function(result) {
												if (result.success) {
													console.log(result);
													$.each(result.operations, function(i, v){
														$('.logged-content > table > tbody').append(
															'<tr><td>'+v.id+'</td><td>'+v.timestamp+'</td><td>'+((v.amount < 0)?'<font color=red>'+v.amount+'</font>':v.amount) +'</td></tr>'
														);
													});
												}
												else {
													console.log(result);
												}
											},
											error: function(request, status, error) {
												console.log('Ошибка: ' + request.responseText);
											}
										});
									});
								});
							});
						}
						else {
							$('.signin-error').show();
							$('.signin-error').html(result.message);
						}
					},
					error: function(request, status, error) {
						console.log('Ошибка: ' + request.responseText);
					}
				});
				return false;
			});
		});
	});
})(jQuery);
